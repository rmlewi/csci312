{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1c7e450d",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 312, Fall 2023 course notes</h1>\n",
    "<h1 style=\"text-align:center;\">File i/o</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8bcc386",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [File i/o](#File-i/o)\n",
    "* [Example](#Example)\n",
    "* [Getting information about a file](#Getting-information-about-a-file)\n",
    "* [Opening a file](#Opening-a-file)\n",
    "* [Reading and writing a file](#Reading-and-writing-a-file)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "611e08d9",
   "metadata": {},
   "source": [
    "# Python vs. C\n",
    "\n",
    "A comparative summary of Python and C concepts in this notebook.  We do not include comparisons with C++ and Java as they both have much more complicated ways of doing i/o.\n",
    "\n",
    "|           | Python          | C            |\n",
    "| :--------:| :-------------: | :----------: |\n",
    "| Get information about a file | `os.stat()` | `stat()` |\n",
    "| Opening a file | ```open()```   | ```fopen()```   |\n",
    "| Read a file | ```read()``` method | ```fread()``` |\n",
    "| Read a line | `readline()` method | `getline()` |\n",
    "| Flush the i/o buffer | ```flush()```   | ```fflush()``` |\n",
    "| Closing a file | ```close()```   | ```fclose()```   |"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "52651bc6",
   "metadata": {},
   "source": [
    "# File i/o\n",
    "\n",
    "File i/o in C is similar to what you've seen in Python:\n",
    "1. You first open a file for reading or writing,\n",
    "2. you then perform your i/o operations, and\n",
    "3. finally you clean up by closing the file.\n",
    "\n",
    "We will show how to use C's \n",
    "[`fopen()`](https://linux.die.net/man/3/fopen), \n",
    "[`fread()`](https://linux.die.net/man/3/fread), and\n",
    "[`fclose()`](https://linux.die.net/man/3/fclose), which work with objects called **file pointers**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fff83be8",
   "metadata": {},
   "source": [
    "# Example\n",
    "\n",
    "We will use the following program to illustrate i/o:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "319b80e4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "     1\t#include <stdio.h>\n",
      "     2\t#include <stdlib.h>\n",
      "     3\t#include <sys/stat.h>\n",
      "     4\t#include <sys/types.h>\n",
      "     5\t#include <unistd.h>\n",
      "     6\t\n",
      "     7\tint main(int argc, char **argv)\n",
      "     8\t{\n",
      "     9\t  int m;\n",
      "    10\t  int n = 1024;\n",
      "    11\t  char *pt = malloc(n * sizeof(char));\n",
      "    12\t  int  *qt = malloc(n * sizeof(int));\n",
      "    13\t  struct stat stat_struct;\n",
      "    14\t\n",
      "    15\t  /* Open the file. */\n",
      "    16\t  FILE *fp = fopen(\"walrus.txt\", \"r\");  /* A file pointer. */\n",
      "    17\t\n",
      "    18\t  /* Get information about the file with stat().  See the man page for stat() for details. */\n",
      "    19\t  stat(\"walrus.txt\", &stat_struct);\n",
      "    20\t  printf(\"size of file (st_size): %lld\\n\\n\", stat_struct.st_size);\n",
      "    21\t\n",
      "    22\t  /* Read 32 bytes from the file. */\n",
      "    23\t  m = fread(pt, sizeof(char), 32, fp);\n",
      "    24\t  printf(\"number of bytes read: %d\\n\", m);\n",
      "    25\t  printf(\"feof: %d\\n\", feof(fp));\n",
      "    26\t  printf(\"ferror: %d\\n\\n\", ferror(fp));\n",
      "    27\t\n",
      "    28\t  /* Try to read 1024 bytes from the file; we hit the end of file (EOF) first. */\n",
      "    29\t  m = fread(qt, sizeof(char), n, fp);\n",
      "    30\t  printf(\"number of bytes read: %d\\n\", m);\n",
      "    31\t  printf(\"feof: %d\\n\", feof(fp));\n",
      "    32\t  printf(\"ferror: %d\\n\\n\", ferror(fp));\n",
      "    33\t\n",
      "    34\t  fclose(fp);  /* Not actually needed since we are about to terminate. */\n",
      "    35\t}\n"
     ]
    }
   ],
   "source": [
    "cat -n fread.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "802f5317",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -Wall -pedantic fread.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d78ff436",
   "metadata": {},
   "source": [
    "The file `walrus.txt` is 190 bytes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "3abe33e2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "8 -rw-------  1 rml  staff  190 Jun 14 11:23 walrus.txt\n"
     ]
    }
   ],
   "source": [
    "ls -ls walrus.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "d3c60a0c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "size of file (st_size): 190\n",
      "\n",
      "number of bytes read: 32\n",
      "feof: 0\n",
      "ferror: 0\n",
      "\n",
      "number of bytes read: 158\n",
      "feof: 1\n",
      "ferror: 0\n",
      "\n"
     ]
    }
   ],
   "source": [
    "./a.out "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "548ab717",
   "metadata": {},
   "source": [
    "# Getting information about a file\n",
    "\n",
    "You can get information about a file using the `stat()` function, which takes as its inputs the pathname of thefile and a pointer for a `stat` structure it populates. Python's [`os.stat()` and `os.stat_result` object returned by `os.stat()`](https://docs.python.org/3/library/os.html?highlight=os#os.stat_result) are based on these."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ec6c4941",
   "metadata": {
    "scrolled": true
   },
   "source": [
    "man 2 stat"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5f869044",
   "metadata": {},
   "source": [
    "Here is the information you can access using `stat()`:\n",
    "\n",
    "<code>\n",
    "struct stat {\n",
    "   dev_t     st_dev;         /* ID of device containing file */\n",
    "   ino_t     st_ino;         /* Inode number */\n",
    "   mode_t    st_mode;        /* File type and mode */\n",
    "   nlink_t   st_nlink;       /* Number of hard links */\n",
    "   uid_t     st_uid;         /* User ID of owner */\n",
    "   gid_t     st_gid;         /* Group ID of owner */\n",
    "   dev_t     st_rdev;        /* Device ID (if special file) */\n",
    "   off_t     st_size;        /* Total size, in bytes */\n",
    "   blksize_t st_blksize;     /* Block size for filesystem I/O */\n",
    "   blkcnt_t  st_blocks;      /* Number of 512B blocks allocated */\n",
    "\n",
    "   /* Since Linux 2.6, the kernel supports nanosecond\n",
    "      precision for the following timestamp fields.\n",
    "      For the details before Linux 2.6, see NOTES. */\n",
    "\n",
    "   struct timespec st_atim;  /* Time of last access */\n",
    "   struct timespec st_mtim;  /* Time of last modification */\n",
    "   struct timespec st_ctim;  /* Time of last status change */\n",
    "}\n",
    "</code>    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42e79c1c",
   "metadata": {},
   "source": [
    "# Opening a file\n",
    "\n",
    "You can open a file for i/o access using `fopen()`: \n",
    "\n",
    "<code>\n",
    "FILE *fp;\n",
    "fp = fopen(\"walrus.txt\", \"r\");  /* Open walrus.txt for read-only. */\n",
    "</code>\n",
    "\n",
    "A call to `fopen()` returns an object called a **FILE pointer** or **i/o stream** which is used to execute i/o operations for the associated file.  If the file cannot be opened, `fopen()` returns a null pointer."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a7244f2",
   "metadata": {},
   "source": [
    "As in Python you can open a file in different **modes**:\n",
    "\n",
    "| Mode | Meaning | Details |\n",
    "| :---: | :---: | :---: |\n",
    "| `r` | Open for reading. | If the file does not exist, `fopen()` returns `NULL`. |\n",
    "| `r+` | Open for reading and writing. | Same as `r` mode. |\n",
    "| `w` | Open for writing. | If the file exists, it is overwritten. If it does not exist, it is created. |\n",
    "| `w+` | Open for both reading and writing. | Same as `w` mode. |\n",
    "| `a` | Open for appending. | Output is added to the end of the file. If the file does not exist, it is created. |\n",
    "\n",
    "There is also a `b` modifer that can be added to any of these modes (e.g., `rb+`).  This indicates the file should be treated as a binary file (i.e., not text).  However, on Unix/Linux systems `b` has no effect:\n",
    "<blockquote>\n",
    "The mode string can also include the letter 'b' either as a last character or as a character between the characters in any of the two-character strings described above.  This is strictly for compatibility with C89 and has no effect; the 'b' is ignored on all POSIX conforming systems, including Linux.  (Other systems may treat text files and binary files\n",
    "differently, and adding the 'b' may be a good idea if you do I/O to a binary file and expect that your program may be ported to non-UNIX environments.) <br/><br/>\n",
    "&ndash; Linux `man` page for `fopen()`.    \n",
    "</blockquote>\n",
    "\n",
    "Compare the C file modes with the file modes [in Python](https://docs.python.org/3/library/functions.html?highlight=open#open).\n",
    "\n",
    "Here is the `man` page for `fopen()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "13f591dc",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "FOPEN(3)                   Library Functions Manual                   FOPEN(3)\n",
      "\n",
      "NAME\n",
      "     fopen, fdopen, freopen, fmemopen – stream open functions\n",
      "\n",
      "LIBRARY\n",
      "     Standard C Library (libc, -lc)\n",
      "\n",
      "SYNOPSIS\n",
      "     #include <stdio.h>\n",
      "\n",
      "     FILE *\n",
      "     fopen(const char * restrict path, const char * restrict mode);\n",
      "\n",
      "     FILE *\n",
      "     fdopen(int fildes, const char *mode);\n",
      "\n",
      "     FILE *\n",
      "     freopen(const char *path, const char *mode, FILE *stream);\n",
      "\n",
      "     FILE *\n",
      "     fmemopen(void *restrict *buf, size_t size, const char * restrict mode);\n",
      "\n",
      "DESCRIPTION\n",
      "     The fopen() function opens the file whose name is the string pointed to\n",
      "     by path and associates a stream with it.\n",
      "\n",
      "     The argument mode points to a string beginning with one of the following\n",
      "     letters:\n",
      "\n",
      "     “r”     Open for reading.  The stream is positioned at the beginning of\n",
      "             the file.  Fail if the file does not exist.\n",
      "\n",
      "     “w”     Open for writing.  The stream is positioned at the beginning of\n",
      "             the file.  Create the file if it does not exist.\n",
      "\n",
      "     “a”     Open for writing.  The stream is positioned at the end of the\n",
      "             file.  Subsequent writes to the file will always end up at the\n",
      "             then current end of file, irrespective of any intervening\n",
      "             fseek(3) or similar.  Create the file if it does not exist.\n",
      "\n",
      "     An optional “+” following “r”, “w”, or “a” opens the file for both\n",
      "     reading and writing.  An optional “x” following “w” or “w+” causes the\n",
      "     fopen() call to fail if the file already exists.  An optional “e”\n",
      "     following the above causes the fopen() call to set the FD_CLOEXEC flag on\n",
      "     the underlying file descriptor.\n",
      "\n",
      "     The mode string can also include the letter “b” after either the “+” or\n",
      "     the first letter.  This is strictly for compatibility with ISO/IEC\n",
      "     9899:1990 (“ISO C90”) and has effect only for fmemopen() ; otherwise “b”\n",
      "     is ignored.\n",
      "\n",
      "     Any created files will have mode “S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP |\n",
      "     S_IROTH | S_IWOTH” (0666), as modified by the process' umask value (see\n",
      "     umask(2)).\n",
      "\n",
      "     Reads and writes may be intermixed on read/write streams in any order;\n",
      "     however, a file positioning function must be called when switching\n",
      "     between output and input, unless an input operation encounters end-of-\n",
      "     file.\n",
      "\n",
      "     The fdopen() function associates a stream with the existing file\n",
      "     descriptor, fildes.  The mode of the stream must be compatible with the\n",
      "     mode of the file descriptor.  The “x” mode option is ignored.  If the “e”\n",
      "     mode option is present, the FD_CLOEXEC flag is set, otherwise it remains\n",
      "     unchanged.  When the stream is closed via fclose(3), fildes is closed\n",
      "     also.\n",
      "\n",
      "     The freopen() function opens the file whose name is the string pointed to\n",
      "     by path and associates the stream pointed to by stream with it.  The\n",
      "     original stream (if it exists) is closed.  The mode argument is used just\n",
      "     as in the fopen() function.\n",
      "\n",
      "     If the path argument is NULL, freopen() attempts to re-open the file\n",
      "     associated with stream with a new mode.  The new mode must be compatible\n",
      "     with the mode that the stream was originally opened with: Streams open\n",
      "     for reading can only be re-opened for reading, streams open for writing\n",
      "     can only be re-opened for writing, and streams open for reading and\n",
      "     writing can be re-opened in any mode.  The “x” mode option is not\n",
      "     meaningful in this context.\n",
      "\n",
      "     The primary use of the freopen() function is to change the file\n",
      "     associated with a standard text stream (stderr, stdin, or stdout).\n",
      "\n",
      "     The fmemopen() function associates the buffer given by the buf and size\n",
      "     arguments with a stream.  The buf argument is either a null pointer or a\n",
      "     pointer to a buffer that is at least size bytes long.  If a null pointer\n",
      "     is specified as the buf argument, fmemopen() allocates size bytes of\n",
      "     memory, and this allocation is automatically freed when the stream is\n",
      "     closed.  If a non-null pointer is specified, the caller retains ownership\n",
      "     of the buffer and is responsible for disposing of it after the stream has\n",
      "     been closed.  Buffers can be opened in text-mode (default) or binary-mode\n",
      "     (if “b” is present in the second or third position of the mode argument).\n",
      "     Buffers opened in text-mode make sure that writes are terminated with a\n",
      "     NULL byte, if the last write hasn't filled up the whole buffer. Buffers\n",
      "     opened in binary-mode never append a NULL byte.\n",
      "\n",
      "     Input and output against the opened stream will be fully buffered, unless\n",
      "     it refers to an interactive terminal device, or a different kind of\n",
      "     buffering is specified in the environment.  See setvbuf(3) for additional\n",
      "     details.\n",
      "\n",
      "RETURN VALUES\n",
      "     Upon successful completion fopen(), fdopen(), freopen() and fmemopen()\n",
      "     return a FILE pointer.  Otherwise, NULL is returned and the global\n",
      "     variable errno is set to indicate the error.\n",
      "\n",
      "ERRORS\n",
      "     [EINVAL]           The mode argument to fopen(), fdopen(), freopen(), or\n",
      "                        fmemopen() was invalid.\n",
      "\n",
      "     The fopen(), fdopen(), freopen() and fmemopen() functions may also fail\n",
      "     and set errno for any of the errors specified for the routine malloc(3).\n",
      "\n",
      "     The fopen() function may also fail and set errno for any of the errors\n",
      "     specified for the routine open(2).\n",
      "\n",
      "     The fdopen() function may also fail and set errno for any of the errors\n",
      "     specified for the routine fcntl(2).\n",
      "\n",
      "     The freopen() function may also fail and set errno for any of the errors\n",
      "     specified for the routines open(2), fclose(3) and fflush(3).\n",
      "\n",
      "     The fmemopen() function may also fail and set errno if the size argument\n",
      "     is 0.\n",
      "\n",
      "SEE ALSO\n",
      "     open(2), fclose(3), fileno(3), fseek(3), funopen(3)\n",
      "\n",
      "STANDARDS\n",
      "     The fopen() and freopen() functions conform to ISO/IEC 9899:1990\n",
      "     (“ISO C90”), with the exception of the “x” mode option which conforms to\n",
      "     ISO/IEC 9899:2011 (“ISO C11”).  The fdopen() function conforms to IEEE\n",
      "     Std 1003.1-1988 (“POSIX.1”).  The “e” mode option does not conform to any\n",
      "     standard but is also supported by glibc.  The fmemopen() function\n",
      "     conforms to IEEE Std 1003.1-2008 (“POSIX.1”).  The “b” mode does not\n",
      "     conform to any standard but is also supported by glibc.\n",
      "\n",
      "macOS 13.5                     January 30, 2013                     macOS 13.5\n"
     ]
    }
   ],
   "source": [
    "man fopen"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd6a4bfd",
   "metadata": {},
   "source": [
    "# Reading and writing a file\n",
    "\n",
    "You can read from a file using `fread()` and write to a file using `fwrite()`.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9cf75f7f",
   "metadata": {},
   "outputs": [],
   "source": [
    "man fread"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a780eefd",
   "metadata": {},
   "source": [
    "Observe that each function reads or writes a specified number of bytes.  For instance, \n",
    "<code>\n",
    "    fread(pt, sizeof(int), n, fp);\n",
    "</code>\n",
    "reads a total of `n * sizeof(int)` bytes into the chunk of memory `pt` points to using the i/o stream defined in the file pointer `fp`.\n",
    "\n",
    "The function `fread()` returns the number of items that were actually read (as opposed to the number of items requested, which is `n` in the preceding call).  E.g., if our file contains 10 `int` and we request a read of 16 `int`, then `fread()` would return 10.  This is one way to tell if we have reached the end of a file, or possibly encountered an error."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "145dd699",
   "metadata": {},
   "source": [
    "## Allocating memory for reading\n",
    "\n",
    "You can see that `fread()` reads a specified number of bytes into a chunk of memory.  You can either\n",
    "1. use `stat()` to figure out in advance how large the file is, and allocate enough memory to read the entire file in one go, or\n",
    "2. pre-allocate a block of memory, and read the file in chunks of that size.\n",
    "\n",
    "In the latter case you may encounter the end of the file, which is OK.  In this situation `fread()` will read as much data as it can and return gracefully."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c5f5f3ef",
   "metadata": {},
   "source": [
    "## End of file and  i/o errors\n",
    "\n",
    "The function `fread()` returns the number of bytes actually read, and `fwrite()` returns the number of bytes actually written.  Either may be smaller than the amount specified in calling them either because\n",
    "1. the end of the file was encountered (EOF), or\n",
    "2. there was an i/o error.\n",
    "\n",
    "For instance, in our example above we encountered EOF when we tried to read 1024 bytes from `walrus.txt`.\n",
    "\n",
    "You can if either occured by calling the functions `feof()` and `ferror()` after the call to `fread()` or `fwrite()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "77135597",
   "metadata": {},
   "outputs": [],
   "source": [
    "man feof"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d5286b24",
   "metadata": {},
   "source": [
    "# Flushing the i/o buffer\n",
    "\n",
    "Typically output is buffered.  In buffered output our output is not instantly written to the file because that would be very inefficient.  Instead, it is written to a chunk of memory called the i/o buffer.  When the i/o buffer is full its contents are written to the file.\n",
    "\n",
    "If you have a program that takes a long time to complete you might want to flush the output buffer using the `fflush()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f9a9977",
   "metadata": {},
   "outputs": [],
   "source": [
    "man fflush"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f7120952",
   "metadata": {},
   "source": [
    "It's a good idea to do this from time-to-time in case your program crashes, since otherwise you will likely lose any unwritten contents of the output buffer."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61c7080d",
   "metadata": {},
   "source": [
    "# Closing the file\n",
    "\n",
    "When you are finished with a file you should close it (it will be automatically closed when the program terminates).  This is because there is a limit to the number of files you can have open at any given time (connections to files require resources).  The Linux `ulimit` command will show you your resource limits:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bf684ba4",
   "metadata": {},
   "outputs": [],
   "source": [
    "ulimit -a  # Show all user resource limits."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee237699",
   "metadata": {},
   "source": [
    "You can close the file with the `fclose()` command.  Closing the file will flush any unwritten output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16fd7825",
   "metadata": {},
   "outputs": [],
   "source": [
    "man fclose"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bafc43ae",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

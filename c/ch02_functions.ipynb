{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1c7e450d",
   "metadata": {},
   "source": [
    "<img src=\"http://www.cs.wm.edu/~rml/images/wm_horizontal_single_line_full_color.png\">\n",
    "\n",
    "<h1 style=\"text-align:center;\">CSCI 312, Fall 2023 notes on C</h1>\n",
    "<h1 style=\"text-align:center;\">Chapter 2</h1>\n",
    "<h1 style=\"text-align:center;\">Functions</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8bcc386",
   "metadata": {},
   "source": [
    "# Contents\n",
    "\n",
    "* [Functions](#Functions)\n",
    "* [Call by value vs. call by reference](#Call-by-value-vs.-call-by-reference)\n",
    "* [Function prototypes](#Function-prototypes)\n",
    "* [Returning multiple values](#Returning-multiple-values)\n",
    "* [The <code class=\"kw\">void</code> type](#The-void-type)\n",
    "* [Implicit returns 😱 🐞](#Implicit-returns-😱-🐞)\n",
    "* [The <code class=\"kw\">static</code> storage class](#static)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "611e08d9",
   "metadata": {},
   "source": [
    "# Python vs. C vs. C++ vs. Java\n",
    "\n",
    "|           | Python          | C            | C++   | Java |\n",
    "| :--------:| :-------------: | :----------: | :---: | :---: |\n",
    "| return a value from a function | `return`   | same | same | same |\n",
    "| type of functions not returning a value |  | `void` | same as C | same as C |\n",
    "| value from implicit returns | `None` |  |  | "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8698d15",
   "metadata": {},
   "source": [
    "# Functions\n",
    "\n",
    "Functions in C/C++/Java are similar to those in Python.  Instead of using indentation, C/C++/Java delimit the body of a function using `{ }`.\n",
    "\n",
    "You should declare the type of a function (i.e., the type of value returned by the function) and the type of all input arguments.  The following declares `square()` to be a function taking an integer input `n` and returning an integer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "682d29fa",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat ch02/square.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41053ae8",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/square.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8d5639c0",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b719b44c",
   "metadata": {},
   "source": [
    "Omitting the return type of a function results in a warning:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9d1648bd",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/untyped_fun.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4617bdc4",
   "metadata": {},
   "outputs": [],
   "source": [
    "clang -c ch02/untyped_fun.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "56b5b815",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -c ch02/untyped_fun.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "acd86668",
   "metadata": {},
   "source": [
    "The reason this is a warning rather than an error is that in the early days of C the default type of a function was `int`, and making the declaration of a function mandatory would break a lot of old code."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e1071ddb",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div><div class=\"danger\"></div>\n",
    "\n",
    "A warning about a missing type specifier frequently points to an error.  Ignore such warnings at your peril."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91b0b092",
   "metadata": {},
   "source": [
    "It is also an error to omit the type of an argument to a function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8d834c55",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/untyped_arg.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "49790b23",
   "metadata": {},
   "outputs": [],
   "source": [
    "clang -Weverything ch02/untyped_arg.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8313a644",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34f671e8",
   "metadata": {},
   "source": [
    "# Call by value vs. call by reference\n",
    "\n",
    "When Python passes an object of a fundamental type (e.g., an `int` or `float`) it passes a **copy** of the value of the variable is passed, rather than the variable itself.  This is called **call by value**.\n",
    "\n",
    "On the other hand, when Python passes more complex objects (e.g., lists, dictionaries) to functions, it passes the actual object itself, and changes made to the object in the function will persist after returning from the function.  This is called **call by reference**.\n",
    "\n",
    "The following Python code illustrates both types of calls:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35b91263",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/call_by.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7670596b",
   "metadata": {},
   "outputs": [],
   "source": [
    "python ch02/call_by.py"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2dd8d6d0",
   "metadata": {},
   "source": [
    "C is strictly call by value---only copies of values of variables are passed to functions, as the following code illustrates:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d30065ee",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/bad_swap.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8aa5dd0b",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/bad_swap.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aed7ac7e",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6e482a90",
   "metadata": {},
   "source": [
    "When we later introduce **pointers** we will see how we can get the effect of call by reference in C."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "849104be",
   "metadata": {},
   "source": [
    "# Function prototypes\n",
    "\n",
    "A **function prototype** is a description of a function, its return type, and its input types that is independent of the actual definition of the function (i.e., the code that makes up the function).   They allow functions to be called without the source code being seen as the compiler will know how to handle them.\n",
    "\n",
    "Function prototypes make it possible to detect mismatches between the expected and actual number and type of inputs, as well as detecting whether a returned value is being misused because of its type. \n",
    "\n",
    "Header files contain lots of function prototypes.  For instance, `stdio.h` contains prototypes for the C i/o functions, while `math.h` contains prototypes for all of the functions in the C math library.\n",
    "\n",
    "A function prototype for `foo()` appears on line 3 of the following program.  When the compiler reaches the call to `foo()` at line 7 it knows about the inputs and outputs of the function and can generate the appropriate code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2160d883",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto1.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8331e804",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/proto1.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b375dac9",
   "metadata": {},
   "source": [
    "Here is an example where the function is in a different file than where it is called:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0865a24d",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto2a.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "99b96436",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto2b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c87dfb1",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/proto2a.c ch02/proto2b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10bc0262",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "263b1f4c",
   "metadata": {},
   "source": [
    "In these situations it is good practice to put the function prototypes in a header file which is included everywhere it might be needed.  Since the header file is ours and not a system header file it appears in `\" \"`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "805c1a30",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto3.h"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4a82049f",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto3a.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "645add23",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto3b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3813db2e",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/proto3a.c ch02/proto3b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30f6ac84",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5fb5390",
   "metadata": {},
   "source": [
    "The inclusion of the function prototype in the file where the function is defined prevents the following situation, where the actual function does not match its prototype:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e68ec417",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto4.h"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2742824f",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto4a.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a493f0e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto4b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9bef3514",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/proto4a.c ch02/proto4b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a34f7174",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6f4a7fa",
   "metadata": {},
   "source": [
    "Before function prototypes were introduced in C one of my favorite errors was to pass the wrong number of type of inputs to functions.  Used properly, function prototypes catch these kinds of mismatches:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4fdf4ff3",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto5.h"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "90f37961",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto5a.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6bbd8a06",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/proto5b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43b10d62",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/proto5a.c ch02/proto5b.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8e8de69f-a563-4ce3-b326-10ddf0d415e7",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9bec9afb",
   "metadata": {},
   "source": [
    "# Returning multiple values\n",
    "\n",
    "Unlike Python, C does not have a simple way of returning multiple values.\n",
    "\n",
    "If all of the values are of the same type, we can pass an array into the function and fill it with the values we wish to return.  C passes arrays to functions the same way Python passes lists and other containers, as a reference.  This means that the array the function sees is the same array the calling code passed, rather than a copy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6157ed7a",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/min_max.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "70402c0a",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/min_max.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a46e697a",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f8c56cb",
   "metadata": {},
   "source": [
    "Later we will see a way of returning multiple values of different types using a type of object called a **structure**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ed3ccb3",
   "metadata": {},
   "source": [
    "# The `void` type\n",
    "\n",
    "If a function does not return a value, its type is <code class=\"kw\">void</code>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c49fd31",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/void1.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "be896592",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -c ch02/void1.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1193ecf6",
   "metadata": {},
   "source": [
    "You should also use <code class=\"kw\">void</code> to indicate that a function has no input arguments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "01abbd5f",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/void2.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "645c17e6",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -c ch02/void2.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "06efb6d7",
   "metadata": {},
   "source": [
    "The compiler should detect an attempt to (mis)use the result of a call to a function of type `void`, or to pass arguments to a function that has no inputs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1adf2ae6",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/void3.c "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c37fb01f",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc -c ch02/void3.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "75603fbf",
   "metadata": {},
   "source": [
    "# Implicit returns 😱 🐞\n",
    "\n",
    "As in Python, reaching the end of a function is implicitly a return.  However, C does not return a default value like Python's <code class=\"kw\">None</code>.  Worse yet, C will let you try to use a returned value when none was returned, in which case there is no telling what will happen.\n",
    "\n",
    "A good compiler will warn you if your code allows for returning from a function without returning a value of the appropriate type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2fc8dcb4",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat -n ch02/implicit_return.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dc01f764",
   "metadata": {},
   "outputs": [],
   "source": [
    "gcc ch02/implicit_return.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "33ebdb45",
   "metadata": {},
   "outputs": [],
   "source": [
    "clang ch02/implicit_return.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed772e8d",
   "metadata": {},
   "source": [
    "In Python you can detect whether you've fallen off the end of a function by checking for a returned value of <code class=\"kw\">None</code>.  There is no analogous test in C.\n",
    "\n",
    "This is why you should keep the logic of your functions as simple as possible.  Complex logic makes it more likely there is a path through a function that falls off the end and surprises you at some point."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f14349e4",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "🐞 Make sure functions that are supposed to return a value always return a value. 🐞"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6b96aabd",
   "metadata": {},
   "source": [
    "In this case the program runs even when we fall off the end of the function and return nothing, but produces the wrong result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "529c0fa4",
   "metadata": {},
   "outputs": [],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2f945f94-1a84-4f21-92f4-65bf95533d8b",
   "metadata": {},
   "source": [
    "<div class=\"danger\"></div>\n",
    "<div class=\"danger\"></div>\n",
    "🐞 😱 If you try to use a returned value when none was returned, Very Bad Things&trade; will happen. 😱 🐞"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "190de4dc-faa6-41d9-89fe-38416903bca7",
   "metadata": {},
   "source": [
    "# The <code class=\"kw\">static</code> storage class <a id=\"static\"/>\n",
    "\n",
    "**Static** variables are persistent variables.  Depending on where they appear in the code, they may have one of two behaviors:\n",
    "1. Inside a function they persist from call to call.\n",
    "2. Outside a function they are persistant and visible to any function in the file where they appear.\n",
    "\n",
    "In the following code there are two static variables.  The variable `n` is static at file scope, while the variable `m` is static at function scope.  Note how `n` is visible inside the functions and its value persists.  The value of `m` also persists between calls to `bar()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "a856e07a-0c11-4ac8-accd-f435c85cae39",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "     1\t#include <stdio.h>\n",
      "     2\t\n",
      "     3\tstatic int n = 42;  /* Initialized to 42 at compile time. */\n",
      "     4\t\n",
      "     5\tvoid foo(void)\n",
      "     6\t{\n",
      "     7\t  printf(\"static int n: %d\\n\\n\", n);\n",
      "     8\t}\n",
      "     9\t\n",
      "    10\tvoid bar(int a)\n",
      "    11\t{\n",
      "    12\t  static int m = 0;  /* Initialized to 0 at compile time. */\n",
      "    13\t\n",
      "    14\t  if (m == 0) {\n",
      "    15\t    m = a;\n",
      "    16\t  }\n",
      "    17\t  printf(\"static int m: %d (local to function)\\n\", m);\n",
      "    18\t  printf(\"static int n: %d (global to file)\\n\\n\", n);\n",
      "    19\t}\n",
      "    20\t\n",
      "    21\tint main(void)\n",
      "    22\t{\n",
      "    23\t  printf(\"static int n: %d (global to file)\\n\\n\", n);\n",
      "    24\t  \n",
      "    25\t  foo();\n",
      "    26\t\n",
      "    27\t  bar(42);\n",
      "    28\t  bar(54);\n",
      "    29\t  bar(54);\n",
      "    30\t\n",
      "    31\t  return 0;\n",
      "    32\t}\n"
     ]
    }
   ],
   "source": [
    "cat -n ch02/static.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "f7d51596-5622-4e5e-871b-8be064f06ec7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[0;1;35mwarning: \u001b[0minclude location '/usr/local/include' is unsafe for cross-compilation [-Wpoison-system-directories]\u001b[0m\n",
      "\u001b[1mch02/static.c:5:6: \u001b[0m\u001b[0;1;35mwarning: \u001b[0m\u001b[1mno previous prototype for function 'foo' [-Wmissing-prototypes]\u001b[0m\n",
      "void foo(void)\n",
      "\u001b[0;1;32m     ^\n",
      "\u001b[0m\u001b[1mch02/static.c:5:1: \u001b[0m\u001b[0;1;30mnote: \u001b[0mdeclare 'static' if the function is not intended to be used outside of this translation unit\u001b[0m\n",
      "void foo(void)\n",
      "\u001b[0;1;32m^\n",
      "\u001b[0m\u001b[0;32mstatic \n",
      "\u001b[0m\u001b[1mch02/static.c:10:6: \u001b[0m\u001b[0;1;35mwarning: \u001b[0m\u001b[1mno previous prototype for function 'bar' [-Wmissing-prototypes]\u001b[0m\n",
      "void bar(int a)\n",
      "\u001b[0;1;32m     ^\n",
      "\u001b[0m\u001b[1mch02/static.c:10:1: \u001b[0m\u001b[0;1;30mnote: \u001b[0mdeclare 'static' if the function is not intended to be used outside of this translation unit\u001b[0m\n",
      "void bar(int a)\n",
      "\u001b[0;1;32m^\n",
      "\u001b[0m\u001b[0;32mstatic \n",
      "\u001b[0m3 warnings generated.\n"
     ]
    }
   ],
   "source": [
    "clang -Weverything ch02/static.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "8183a270-5a49-49e9-be02-56e79d5078b1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "static int n: 42 (global to file)\n",
      "\n",
      "static int n: 42\n",
      "\n",
      "static int m: 42 (local to function)\n",
      "static int n: 42 (global to file)\n",
      "\n",
      "static int m: 42 (local to function)\n",
      "static int n: 42 (global to file)\n",
      "\n",
      "static int m: 42 (local to function)\n",
      "static int n: 42 (global to file)\n",
      "\n"
     ]
    }
   ],
   "source": [
    "./a.out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77164bb4-8a6b-4bc2-9c55-5ad3704d2a86",
   "metadata": {},
   "source": [
    "If a function is declared <code class=\"kw\">static</code> then it is only visible inside the file where it is defined:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "b50d2235-e239-4c77-8b07-626be7da3c00",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "     1\tvoid bar(void);\n",
      "     2\t\n",
      "     3\tint main(void)\n",
      "     4\t{\n",
      "     5\t  bar();\n",
      "     6\t  return 0;\n",
      "     7\t}\n"
     ]
    }
   ],
   "source": [
    "cat -n ch02/foo.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "b75e2e7f-8539-4161-91a0-d29747675ded",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "     1\tstatic void bar(void)\n",
      "     2\t{\n",
      "     3\t  int n = 42;\n",
      "     4\t}\n"
     ]
    }
   ],
   "source": [
    "cat -n ch02/bar.c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "c514f29a-4138-453c-bb4f-69ae3d538477",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Undefined symbols for architecture x86_64:\n",
      "  \"_bar\", referenced from:\n",
      "      _main in foo-a7f0a8.o\n",
      "ld: symbol(s) not found for architecture x86_64\n",
      "clang: \u001b[0;1;31merror: \u001b[0mlinker command failed with exit code 1 (use -v to see invocation)\u001b[0m\n"
     ]
    },
    {
     "ename": "",
     "evalue": "1",
     "output_type": "error",
     "traceback": []
    }
   ],
   "source": [
    "clang ch02/foo.c ch02/bar.c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d7e2b692-2de5-4b9f-a411-846a9f6693d4",
   "metadata": {},
   "source": [
    "We get a linkage error because the code in `foo.c` calls a function named `bar()`, but `bar()` in `bar.c` is not visible to code in other files."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

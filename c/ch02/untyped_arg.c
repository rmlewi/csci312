#include <stdio.h>

int bar(n)  /* No type declared for argument n. */
{
  printf("%lu\n", sizeof(n));
    return n * n;
}

int main(void)
{
  printf("%d\n", bar(42.0));

  return 0;
}

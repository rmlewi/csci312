#include <stdio.h>

int foo(int n);  /* Function prototype. */

int main(int argc, char **argv)
{
  printf("42**2 = %d\n", foo(42));
  return 0;
}

int foo(int n)
{
    if (n > 42) return n * n;

    /* If n <= 42 we hit the end of the function and return nothing. */
}

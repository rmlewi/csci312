#include <stdio.h>

static int n = 42;  /* Initialized to 42 at compile time. */

void foo(void)
{
  printf("static int n: %d\n\n", n);
}

void bar(int a)
{
  static int m = 0;  /* Initialized to 0 at compile time. */

  if (m == 0) {
    m = a;
  }
  printf("static int m: %d (local to function)\n", m);
  printf("static int n: %d (global to file)\n\n", n);
}

int main(void)
{
  printf("static int n: %d (global to file)\n\n", n);
  
  foo();

  bar(42);
  bar(54);
  bar(54);

  return 0;
}

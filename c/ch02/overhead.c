#include <stdio.h>
#include <time.h>

double square(double);

int main(int argc, char **argv)
{
  double x = 42;
  double y;
  time_t t_start, t_end;
  long int n = 1000000000;
  long int i;

  printf("Inline...");
  time(&t_start);
  for (i = 0; i < n; i++) {
    y = x*x;
  }
  time(&t_end);
  printf("%ld sec\n\n", t_end - t_start);

  printf("Function call...");
  time(&t_start);
  for (i = 0; i < n; i++) {
    y = square(x);
  }
  time(&t_end);
  printf("%ld sec\n\n", t_end - t_start);
}  

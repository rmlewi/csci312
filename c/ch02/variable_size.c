void foo(int n)
{
  double x[n];
}

int main(void)
{
  int n = 100000000;
  foo(n);

  return 0;
}

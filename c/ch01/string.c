#include <stdio.h>

int main(void)
{
  char c = 'a';    // Character literal.
  char s[] = "a";  // String as an array of characters.

  printf("sizeof(c): %lu bytes\n", sizeof(c));
  printf("sizeof(s): %lu bytes\n", sizeof(s));
  
  return 0;
}

#include <stdio.h>

int main(int argc, char **argv)
{
    int n;

    /* sizeof() applied to types. */
    printf("Sizes (in bytes) of different flavors of integers:\n");
    printf("A char is           %lu byte long.\n", sizeof(char));
    printf("A signed char is    %lu byte long.\n", sizeof(signed char));
    printf("An unsigned char is %lu byte long.\n\n", sizeof(unsigned char));

    printf("A short int is           %lu bytes long.\n", sizeof(short int));
    printf("An unsigned short int is %lu bytes long.\n\n", sizeof(unsigned short int));

    printf("An int is          %lu bytes long.\n", sizeof(int));
    printf("An unsigned int is %lu bytes long.\n\n", sizeof(unsigned int));

    printf("A long int is           %lu bytes long.\n", sizeof(long int));
    printf("An unsigned long int is %lu bytes long.\n\n", sizeof(unsigned long int));

    printf("A long long int is           %lu bytes long.\n", sizeof(long long int));
    printf("An unsigned long long int is %lu bytes long.\n\n", sizeof(unsigned long long int));

    printf("Sizes (in bytes) of different flavors of floating-point numbers:\n");
    printf("A float is       %lu bytes long.\n", sizeof(float));
    printf("A double is      %lu bytes long.\n", sizeof(double));
    printf("A long double is %lu bytes long.\n\n", sizeof(long double));

    printf("A pointer is %lu bytes long.\n", sizeof(int*));
    return 0;
}

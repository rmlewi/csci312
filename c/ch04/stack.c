#include <stdio.h>

void foo(void)
{
  double x[2000000];
  x[0] = 42.0;
  return;
}

int main(void)
{
  fprintf(stderr, "Calling foo()...\n");
  foo();
  return 0;
}
